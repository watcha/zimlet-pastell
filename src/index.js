import { createElement } from 'preact';
import withIntl from './enhancers';
import PastellOptions from './components/pastell-options';

//Load a style static stylesheet (Preact will not change this)
import './public/styles.css';

export default function Zimlet(context) {
	const { plugins } = context;
	const exports = {};

	exports.init = function init() {
		const options = createOptions(context);
		//plugins.register('slot::compose-sender-options-menu', options);
		plugins.register('slot::mail-composer-toolbar-send', options);
	};
	return exports;
}

function createOptions(context) {
	return props => (<PastellOptions {...props}>{{context}}</PastellOptions>);
}
