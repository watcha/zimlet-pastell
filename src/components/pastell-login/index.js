import { createElement } from 'preact';
import style from './style.less';
import { withText } from 'preact-i18n';
import { ModalDialog } from '@zimbra-client/components';
import { LOGIN_MODAL } from '../../constants';
import withIntl from '../../enhancers';
import axios from 'axios';

function LoginModal(
	{
        loginDialogTitle,
        loginDialogDesc,
        loginDialogLogin,
        loginDialogPassword,
        contextZimlet
	},
	context
) {
	
    const handleSaveClick = e => {
        const authToken = btoa(parent.document.getElementById("input-login-pastell").value+":"+parent.document.getElementById("input-password-pastell").value);
        var reqdata = {};
        reqdata["Body"] = {"ModifyPropertiesRequest":{"_jsns":"urn:zimbraAccount", "prop":{"zimlet":"pastell-zimlet", "name":"auth", "_content":authToken}}};
        reqdata["Header"] = {"context":{"_jsns":"urn:zimbra","csrfToken": contextZimlet.zimbra.getCsrfToken()}};
        const headers = {'Content-Type': 'text/json'};
    
        //Get account infos
        axios.post('/service/soap/ModifyPropertiesRequest', reqdata, { headers })
        .then(res => {
        })

        const { addModal } = contextZimlet.zimletRedux.actions.zimlets;
		const { dispatch } = contextZimlet.store;
		return e && true && dispatch(addModal({ id: LOGIN_MODAL }));
    }
    
    const handleClose = e => {
        const { addModal } = contextZimlet.zimletRedux.actions.zimlets;
        const { dispatch } = contextZimlet.store;
        return e && true && dispatch(addModal({ id: LOGIN_MODAL }));
    }

	return (
		<ModalDialog
			class={style.LoginModal}
			title="pastellSend.loginDialogTitle"
			cancelLabel="buttons.cancel"
			actionLabel="buttons.ok"
			onClose={handleClose}
			onAction={handleSaveClick}
			headerClass={style.header}
			contentClass={style.content}
			footerClass={style.footer}
			disableOutsideClick
		>
            <div>
                {loginDialogDesc}
                <table>
                    <tbody>
                        <tr>
                            <td>{loginDialogLogin}</td>
                            <td><input type="text" id="input-login-pastell" class="blocks_button blocks_button_regular">{loginDialogLogin}</input></td>
                        </tr>
                        <tr>
                            <td>{loginDialogPassword}</td>
                            <td><input type="password" id="input-password-pastell" class="blocks_button blocks_button_regular">{loginDialogPassword}</input></td>
                        </tr>
                    </tbody>
                </table>
            </div>
		</ModalDialog>
	);
}

export default withIntl()(withText({
    loginDialogTitle: 'pastellSend.loginDialogTitle',
    loginDialogDesc: 'pastellSend.loginDialogDesc',
    loginDialogLogin: 'pastellSend.loginDialogLogin',
	loginDialogPassword: 'pastellSend.loginDialogPassword'
})(LoginModal));
