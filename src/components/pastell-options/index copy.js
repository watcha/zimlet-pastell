import { createElement } from 'preact';
import { Text } from 'preact-i18n';
import { withText } from 'preact-i18n';
import {
	ActionMenuGroup,
	ActionMenuItem
} from '@zimbra-client/components';
import { LOGIN_MODAL } from '../../constants';
import { callWith } from '@zimbra-client/util';
import LoginModal from '../pastell-login';
import withIntl from '../../enhancers';
import axios from 'axios';


function PastellOptions({
	getMessage,
	getComposer,
	sentMsg,
	sentMsgError,
	loginMsgError,
	context
}) {

	console.log(this);
	console.log(getComposer());
	//window.top.testobject.store.dispatch(window.top.testobject.zimletRedux.actions.email.closeCompose())
	const zimlets = context.getAccount().zimlets;
	var accountAddress = context.getAccount().name;
	const zimlet = zimlets.zimlet.find(zimlet => zimlet.zimlet[0].name === "pastell-zimlet");
	var pastellServer = "";
	var zimbraInternalUrl = "";
	if (zimlet) {
		const pastellConfig = zimlet.zimletConfig[0].global[0].property || [];
		for (var i = 0; i < pastellConfig.length; i++) {
			if(pastellConfig[i].name == "pastellServer") {
				pastellServer = pastellConfig[i].content;
			}
			if(pastellConfig[i].name == "zimbraInternalUrl") {
				zimbraInternalUrl = pastellConfig[i].content;
			}
		};
	}

	var contextZimlet = context;
	var modal;
	var { addModal } = context.zimletRedux.actions.zimlets;
	
	var notify = message => {
		const { dispatch } = contextZimlet.store;
		dispatch(
			contextZimlet.zimletRedux.actions.notifications.notify({
				message
			})
		);
	};

	const sendPastell = () => {
		
		var authToken = "";
		var reqdata = {};
		reqdata["Body"] = {"GetInfoRequest":{"_jsns":"urn:zimbraAccount"}};
		reqdata["Header"] = {"context":{"_jsns":"urn:zimbra","csrfToken": context.zimbra.getCsrfToken()}};
	
		const headers = {'Content-Type': 'text/json'};
		//Get account infos
		axios.post('/service/soap/GetInfoRequest', reqdata, { headers })
		.then(res => {
			let accountInfo = res.data.Body.GetInfoResponse;
			const zimletProps = accountInfo.props.prop;
			if(zimletProps !== undefined) {
				const items = zimletProps.map(prop => {
					if (prop.zimlet === "pastell-zimlet" && prop.name === "auth") {
						authToken = prop._content;
					}
				});
		  	}

			var loginFormdata = new FormData();
			loginFormdata.append('jsondata', JSON.stringify({
				'action' : 'login',
				'server' : pastellServer,
				'zimbraserver' : zimbraInternalUrl,
				'auth' : 'Basic ' + authToken
			}));
			const headers = {
				'Content-Type': 'multipart/form-data',
			};
			axios.post('/service/extension/pastell/', loginFormdata, { headers })
			.then(res => {
				if(res.data == 200) {
					//send mail to pastell
					var msg = getMessage();
					var pastellExtFormdata = new FormData();
					//Get mail data
					pastellExtFormdata.append('jsondata', JSON.stringify({
						'action' : 'sendmsg',
						'text' : msg.text,
						'subject' : msg.subject,
						'to' : getAdresses(msg.to),
						'cc' : getAdresses(msg.cc),
						'bcc' : getAdresses(msg.bcc),
						'files' : JSON.stringify(getAttachments(msg.attachments, accountAddress)),
						'server' : pastellServer,
						'zimbraserver' : zimbraInternalUrl,
						'auth' : 'Basic ' + authToken
					}));

					const headers = { 
						'Content-Type': 'multipart/form-data',
					};

					//Send to  pastell extension to send the message
					axios.post('/service/extension/pastell/', pastellExtFormdata, { headers })
					.then(res => {
						var jsonObj = eval("(" + res.data + ")");
						if(jsonObj.error == "success"){
							notify(sentMsg);
						}
						else {
							notify(sentMsgError);
						}
					});
				}
				else if(res.data == 401) {
					//Open login modal
					const openModal = (modalId) => {
						const { dispatch } = contextZimlet.store;
						modal = <LoginModal
							contextZimlet={contextZimlet}
						/>
						dispatch(addModal({ id: modalId, modal: modal }));
					}

					openModal(LOGIN_MODAL);
				}
				else {
					//error occured with login request other than auth failed
					notify(loginMsgError);
				} 
			});
		})
	}

	return (
		<ActionMenuGroup>
			<ActionMenuItem icon="pastell" onClick={callWith(sendPastell)}>
				<Text id="pastellSend.pastelBtnLabel" />
			</ActionMenuItem>
		</ActionMenuGroup>
	);
}

function getAdresses(adresses) {
	var addrs = "";
	for (const i in adresses) {
		if(addrs != "") {
			addrs += ", ";
		}
		addrs += adresses[i].address;
	}
	return addrs;
}

function getAttachments(attachments, accountAddress) {
	var atts = {};
	for (const i in attachments) {
		atts[attachments[i].filename] = "/service/home/" + accountAddress + "/?auth=qp&id="+attachments[i].messageId+"&part="+attachments[i].part;
		//attachments[i].url.replace('~', accountAddress);
	}
	return atts;
}

export default withIntl()(withText({
	sentMsg: 'pastellSend.sentMsg',
	sentMsgError: 'pastellSend.sentMsgError',
	loginMsgError: 'pastellSend.loginMsgError'
})(PastellOptions));