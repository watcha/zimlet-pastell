import { createElement, Component, render } from 'preact';
import { Text } from 'preact-i18n';
import withIntl from '../../enhancers';
import style from './style';
import { withText } from 'preact-i18n';
import { Button } from '@zimbra-client/blocks';
import { LOGIN_MODAL } from '../../constants';
import LoginModal from '../pastell-login';
import axios from 'axios';


@withIntl()
@withText({
	sentMsg: 'pastellSend.sentMsg',
	sentMsgError: 'pastellSend.sentMsgError',
	loginMsgError: 'pastellSend.loginMsgError',
	title: 'pastellSend.pastelBtnLabel'
})
export default class PastellOptions extends Component {

    constructor(props) {
        super(props);
        this.zimletContext = props.children.context;
    };

    handleClick = e => {

		//console.log(this.props);
		//console.log(this.props.getMessageToSend());

		const zimlets = this.zimletContext.getAccount().zimlets;
		var accountAddress = this.zimletContext.getAccount().name;
		const zimlet = zimlets.zimlet.find(zimlet => zimlet.zimlet[0].name === "pastell-zimlet");
		var pastellServer = "";
		var zimbraInternalUrl = "";
		if (zimlet) {
			const pastellConfig = zimlet.zimletConfig[0].global[0].property || [];
			for (var i = 0; i < pastellConfig.length; i++) {
				if(pastellConfig[i].name == "pastellServer") {
					pastellServer = pastellConfig[i].content;
				}
				if(pastellConfig[i].name == "zimbraInternalUrl") {
					zimbraInternalUrl = pastellConfig[i].content;
				}
			};
		}

		var modal;
		var { addModal } = this.zimletContext.zimletRedux.actions.zimlets;

		var notify = message => {
			const { dispatch } = this.zimletContext.store;
			dispatch(
				this.zimletContext.zimletRedux.actions.notifications.notify({
					message
				})
			);
		};

		//folder sent
		var folderId = "5";

		var authToken = "";

		//Get Folder for storing messages
		var reqdata = {};
		reqdata["Body"] = {"GetFolderRequest":{"_jsns":"urn:zimbraMail","folder":{"path":"/Dossier envoi sécurisé"}}};
		reqdata["Header"] = {"context":{"_jsns":"urn:zimbra","csrfToken": this.zimletContext.zimbra.getCsrfToken()}};

		const headers = {'Content-Type': 'text/json'};
		//Get account infos
		axios.post('/service/soap/GetFolderRequest', reqdata, { headers })
		.then(res => {
			folderId = res.data.Body.GetFolderResponse.folder[0].id;
		})
		.catch(err => {
			var reqdata = {};
			reqdata["Body"] = {"CreateFolderRequest":{"_jsns":"urn:zimbraMail","folder":{"color":null,"name":"Dossier envoi sécurisé","url":null,"view":"message","f":null,"fie":null,"l":"1"}}};
			reqdata["Header"] = {"context":{"_jsns":"urn:zimbra","csrfToken": this.zimletContext.zimbra.getCsrfToken()}};

			const headers = {'Content-Type': 'text/json'};
			//Get account infos
			axios.post('/service/soap/CreateFolderRequest', reqdata, { headers })
			.then(res => {
				folderId = res.data.Body.CreateFolderResponse.folder[0].id;
			});
		});

		var reqdata = {};
		reqdata["Body"] = {"GetInfoRequest":{"_jsns":"urn:zimbraAccount"}};
		reqdata["Header"] = {"context":{"_jsns":"urn:zimbra","csrfToken": this.zimletContext.zimbra.getCsrfToken()}};

		//const headers = {'Content-Type': 'text/json'};
		//Get account infos
		axios.post('/service/soap/GetInfoRequest', reqdata, { headers })
		.then(res => {
			let accountInfo = res.data.Body.GetInfoResponse;
			const zimletProps = accountInfo.props.prop;
			if(zimletProps !== undefined) {
				const items = zimletProps.map(prop => {
					if (prop.zimlet === "pastell-zimlet" && prop.name === "auth") {
						authToken = prop._content;
					}
				});
		  	}

			var loginFormdata = new FormData();
			loginFormdata.append('jsondata', JSON.stringify({
				'action' : 'login',
				'server' : pastellServer,
				'zimbraserver' : zimbraInternalUrl,
				'auth' : 'Basic ' + authToken
			}));
			const headers = {
				'Content-Type': 'multipart/form-data',
			};
			axios.post('/service/extension/pastell/', loginFormdata, { headers })
			.then(res => {
				if(res.data == 200) {
					//send mail to pastell
					var msg = this.props.getMessageToSend();
					var pastellExtFormdata = new FormData();
					//Get mail data
					pastellExtFormdata.append('jsondata', JSON.stringify({
						'action' : 'sendmsg',
						'msgid' : msg.draftId,
						'text' : msg.text,
						'subject' : msg.subject,
						'to' : getAdresses(msg.to),
						'cc' : getAdresses(msg.cc),
						'bcc' : getAdresses(msg.bcc),
						'files' : JSON.stringify(getAttachments(msg.attachments, accountAddress)),
						'server' : pastellServer,
						'zimbraserver' : zimbraInternalUrl,
						'auth' : 'Basic ' + authToken
					}));

					const headers = {
						'Content-Type': 'multipart/form-data',
					};

					//Send to  pastell extension to send the message
					axios.post('/service/extension/pastell/', pastellExtFormdata, { headers })
					.then(res => {
						var jsonObj = eval("(" + res.data + ")");
						if(jsonObj.error == "success"){
							notify(this.props.sentMsg);
							this.props.closeComposer();

							var reqdata = {};
							reqdata["Body"] = {"MsgActionRequest":{"_jsns":"urn:zimbraMail","action":{"id":msg.draftId,"l":folderId,"op":"move"}}};
							reqdata["Header"] = {"context":{"_jsns":"urn:zimbra","csrfToken": this.zimletContext.zimbra.getCsrfToken()}};

							const headers = {'Content-Type': 'text/json'};
							//Get account infos
							axios.post('/service/soap/MsgActionRequest', reqdata, { headers });
							window.top.location = "/modern/email/Dossier envoi sécurisé";
						}
						else {
							notify(this.props.sentMsgError);
						}
					});
				}
				else if(res.data == 401) {
					//Open login modal
					const openModal = (modalId) => {
						const { dispatch } = this.zimletContext.store;
						modal = <LoginModal
							contextZimlet={this.zimletContext}
						/>
						dispatch(addModal({ id: modalId, modal: modal }));
					}

					openModal(LOGIN_MODAL);
				}
				else {
					//error occured with login request other than auth failed
					notify(this.props.loginMsgError);
				}
			});
		})
	}

    render() {
        const childIcon = (
            <span class={style.appIcon}>
            </span>);

        return (
            <Button
                class={style.button}
                onClick={this.handleClick}
                brand="primary"
                icon={childIcon}
            >
                {this.props.title}
            </Button>
        );
    }
}

function getAdresses(adresses) {
	var addrs = "";
	for (const i in adresses) {
		if(addrs != "") {
			addrs += ", ";
		}
		addrs += adresses[i].address;
	}
	return addrs;
}

function getAttachments(attachments, accountAddress) {
	var atts = {};
	for (const i in attachments) {
		atts[attachments[i].filename] = "/service/home/" + accountAddress + "/?auth=qp&id="+attachments[i].messageId+"&part="+attachments[i].part;
		//attachments[i].url.replace('~', accountAddress);
	}
	return atts;
}
